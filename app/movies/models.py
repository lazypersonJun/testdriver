from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser, User


class CustomUser(AbstractUser):
    pass
